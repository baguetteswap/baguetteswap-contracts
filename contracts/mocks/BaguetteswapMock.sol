// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

import "../Baguetteswap.sol";


contract FactoryMock is IFactory {
    uint256 private _fee;

    function fee() external view override returns(uint256) {
        return _fee;
    }

    function setFee(uint256 newFee) external {
        _fee = newFee;
    }
}


contract BaguetteswapMock is Baguetteswap {
    constructor(IERC20[] memory assets, string memory name, string memory symbol)
        public Baguetteswap(assets, name, symbol)
    {
        factory = new FactoryMock();
    }
}
