const Migrations = artifacts.require('./Migrations.sol');
const BaguetteFactory = artifacts.require('./BaguetteFactory.sol');
// const Baguetteswap = artifacts.require('./Baguetteswap.sol');

module.exports = function (deployer) {
    deployer.deploy(Migrations);
    deployer.deploy(BaguetteFactory);
    // deployer.deploy(Baguetteswap);
};
